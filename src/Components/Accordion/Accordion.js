import React, { useState, useRef } from "react";
import Chevron from "../Chevron/Chevron";
import { useSelector } from "react-redux";
import "./Accordion.css";
import Bar from "../Bar/Bar";
import AccordionContent from "../AccordionContent/AccordionContent";

function Accordion(props) {
  const [setActive, setActiveState] = useState("");
  const [setHeight, setHeightState] = useState("0px");
  const [setRotate, setRotateState] = useState("accordion__icon");

  const content = useRef(null);

  const store = useSelector(store => store);

  const timelineWidth = store.home.timelineWidth;
  function toggleAccordion() {
    setActiveState(setActive === "" ? "active" : "");
    setHeightState(
      setActive === "active" ? "0px" : `${content.current.scrollHeight}px`
    );
    setRotateState(
      setActive === "active" ? "accordion__icon" : "accordion__icon rotate"
    );
  }

  function getColor(title) {
    switch (title) {
      case "social":
        return "#030b2b";
      case "search":
        return "#0099f9";
      case "display":
        return "#ff2086";
      default:
        return;
    }
  }

  return (
    <span className="accordion__section">
      <thead
        className={`accordion ${setActive} "accordion__section"`}
        onClick={toggleAccordion}
      >
        <tr>
          <th className="thstyle">
            <span id="title">{props.title.toUpperCase()}</span>
            <span className="chev ">
              <Chevron className={`${setRotate}`} width={10} fill={"#777"} />
            </span>
          </th>

          <th
            className={`accordion ${setActive}`}
            style={{ minWidth: `${timelineWidth}px` }}
          >
            <Bar
              color={getColor(props.title)}
              timeline={props.content.timeline}
              timelineWidth={timelineWidth - 150}
            />
          </th>
        </tr>
      </thead>

      <tbody
        ref={content}
        style={{ maxHeight: `${setHeight}` }}
        className="accordion__content"
      >
        <tr style={{ textAlign: "left" }}>
          <AccordionContent
            data={props.content}
            timelineWidth={timelineWidth - 150}
          />
        </tr>
      </tbody>
    </span>
  );
}

export default Accordion
