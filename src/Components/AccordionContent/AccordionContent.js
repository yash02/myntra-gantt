import React from "react";
import Bar from "../Bar/Bar";
import "./AccordionContent.css";

function AccordionContent(props) {
  return (
    <span>
      {Object.keys(props.data.entities[0]).map((key, index) => {
        return (
          <span key={`${key}`}>
            <td id="subtitle">{key}</td>
            <td style={{ minWidth: `${props.timelineWidth}px` }}>
              <Bar
                timeline={props.data.entities[0][key]}
                timelineWidth={props.timelineWidth}
              />
            </td>
          </span>
        );
      })}
    </span>
  );
}

export default AccordionContent
