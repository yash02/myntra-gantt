import React, { Component } from "react";
import _ from "lodash";
import moment from "moment";
import { connect } from "react-redux";


class Bar extends Component {
  state = {
    leftBound: moment()
      .set({ hour: 0, date: 1, month: 5, year: 2018 })
      .toDate(),
    rightBound: moment()
      .set({ hour: 0, date: 30, month: 4, year: 2019 })
      .toDate(),
  };

  componentDidMount() {

  }

  getSteps() {
    return _.map(this.props.timeline, (step, index) => {
      return this.getStep(step, index);
    });
  }

  getColor(){
    if( this.props.color){
     return  this.props.color
    }
   return  "repeating-linear-gradient(55deg,rgb(208,206,206), rgba(221, 219, 220, 1) 5px, rgb(144, 132, 132) 5px, rgb(156, 145, 145) 5px)"
  }

  getStep(step, index) {
    const { leftBound } = this.state;

    let stepDuration = 0;
    let startDays = 0;
    let endDays = 0;
    let startPixelDays = 0;
    let startPixel = 0;
    let endPixel = 0;
    let displayWidth = 0;

    if (index === 1) {
      startDays = moment(
        moment(this.props.timeline[1].startDate).format("YYYY-MM-DD")
      ).diff(moment(leftBound).format("YYYY-MM-DD"), "days");
      endDays = moment(
        moment(this.props.timeline[1].endDate).format("YYYY-MM-DD")
      ).diff(moment(leftBound).format("YYYY-MM-DD"), "days");

      let prevdaysToLeftBound = moment(
        moment(this.props.timeline[0].endDate).format("YYYY-MM-DD")
      ).diff(moment(leftBound).format("YYYY-MM-DD"), "days");

      startPixelDays = startDays - prevdaysToLeftBound;
      stepDuration = endDays - startDays;
      startPixel = this.timeToPixel(startPixelDays);
      endPixel = this.timeToPixel(endDays);
      let startWidth = this.timeToPixel(startDays);
      displayWidth = endPixel - startWidth ;
    } else {
      const { startDate, endDate } = step;

      startDays = moment(moment(startDate).format("YYYY-MM-DD")).diff(
        moment(leftBound)
          .subtract(1, "months")
          .format("YYYY-MM-DD"),
        "days"
      );
      endDays = moment(moment(endDate).format("YYYY-MM-DD")).diff(
        moment(leftBound)
          .subtract(1, "months")
          .format("YYYY-MM-DD"),
        "days"
      );

      stepDuration = endDays - startDays;
      startPixelDays = startDays;
      startPixel = this.timeToPixel(startDays)+20;
      endPixel = this.timeToPixel(endDays);
      displayWidth = endPixel - startPixel;
    }

    const theoreticalWidth = this.durationToWidth(stepDuration);

    return {
      duration: stepDuration,
      theoreticalWidth,
      displayWidth,
      startPixel,
      endPixel,
      color: "#rrr"
    };
  }

  durationToWidth(duration) {
    const { leftBound, rightBound } = this.state;
    const { timelineWidth } = this.props;
    const timelineDuration = moment(rightBound).diff(leftBound, "days");
    const span = ((timelineWidth) / timelineDuration) * duration;
    return span;
  }

  timeToPixel(time) {
    const { timelineWidth } = this.props;
    const leftBoundPixel = 0;
    const rightBoundPixel = timelineWidth;

    const timeWidthFromLeftBound = this.durationToWidth(time);

    const pixel = timeWidthFromLeftBound;
    if (leftBoundPixel < pixel && pixel < rightBoundPixel) return pixel;
    if (pixel <= leftBoundPixel) return leftBoundPixel;
    if (pixel >= rightBoundPixel) return rightBoundPixel;
    return null;
  }

  defaultRender() {
    const steps = this.getSteps();
    const { timelineWidth } = this.props;
    const color = this.getColor();
        return (
      <span ref="bar" style={{ display: "flex", maxWidth: `${timelineWidth+11}` }}>
        {_.map(steps, (step, index) => {
          return (
            <span key={`${index}`}>
              <span
                style={{
                  minHeight: "23px",
                  display: "inline-block",
                  borderLeft: "1px solid black",
                  borderRight: "1px solid black",
                  width: `${step.displayWidth}px`,
                  backgroundColor: step.color,
                  marginLeft: `${step.startPixel}px`,
                  background: `${color}`
                }}
              ></span>
            </span>
          );
        })}
      </span>
    );
  }

  render() {
    return this.defaultRender();
  }
}

const mapStateToProps = state => {
  return {
    data: state.home.data,
    timelineWidth: state.home.timelineWidth
  };
};

const mapDispatchToProps = dispatch => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Bar);
