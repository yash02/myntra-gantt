import React from "react";
import Accordion from "../Accordion/Accordion";
import { connect } from "react-redux";
import "./Home.css";
import { fetchList, setTimelineWidth, resetTimelineWidth,setAccordionContent, setAccTitle } from "./homeAction";
import Timeline from "../Timeline/Timeline";
// import MenuTitle from '../MenuTitle/MenuTitle'

class Home extends React.Component {
  componentDidMount() {
    this.props.fetchLists();
    this.resizeEventListener = window.addEventListener("resize", e =>
      this.handleResize(e)
    );
    this.handleResize();
  }

  componentWillUnmount() {
    if (this.resizeEventListener) {
      this.resizeEventListener.removeEventListener();
    }
  }

  handleResize() {
    let width = this.refs.timeline.clientWidth;
    this.props.setWidth(width-150);
  }

  render() {
    const thStyle = { whiteSpace: "nowrap" };

    const { data, isLoading, timelineWidth } = this.props;
    console.log('isLoading',isLoading)
    if (isLoading === true) {
      return <div>Loading...</div>;
    }
    return (
      <div className="App">
        <div className="timelineRef" ref="timeline">
          <table style={{ width: "100%", height: "200px" }} cellSpacing={0}>
            <thead>
              <tr>
                <th
                  style={{
                    paddingLeft: '149px',
                    borderBottom: '1px solid rgb(218, 218, 218)',
                    borderRight: '1px solid rgb(218, 218, 218)',
                  }}
                />
                <th
                  style={{
                    ...thStyle,
                    maxWidth: `${timelineWidth - 11}px`,
                    borderBottom: '1px solid #dadada'
                  }}
                >
                  <Timeline/>
                </th>
              </tr>
            </thead>
          </table>
        </div>
        {data.types &&
          Object.keys(data.types).map((key, index) => {
            
            return (
              <table
                key={index}
                style={{
                  borderCollapse: "collapse",
                  tableLayout: "fixed",
                  minWidth: `${timelineWidth}px`
                }}
              >
                <Accordion
                  title={key}
                  content={data.types[key]}
                />
              </table>
            );
          })}
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    isLoading: state.home.isLoading,
    data: state.home.data,
    error: state.home.error,
    timelineWidth: state.home.timelineWidth
  };
};

const mapDispatchToProps = dispatch => ({
  fetchLists: () => dispatch(fetchList()),
  setWidth: width => dispatch(setTimelineWidth(width)),
  resetWidth: e => dispatch(resetTimelineWidth(e)),
  setAccordionContent: content => dispatch(setAccordionContent(content)),
  setAccTitle: title => dispatch(setAccTitle(title))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
