const fetchList = () => ({
    type: "FETCH_LIST"
  });
  
  const fetchListSuccess = data => ({
    type: "FETCH_LIST_SUCCESS",
    data
  });
  
  const fetchListError = error => ({
    type: "FETCH_LIST_ERROR",
    error
  });

  const setTimelineWidth = width =>({
    type: 'SET_WIDTH',
    width
  })

  const resetTimelineWidth = () => ({
    type: 'RESET_WIDTH',
    
  })

  const setAccordionContent = (content) =>({
    type:'SET_ACCORDION_CONTENT',
    content
  })

  const setAccTitle = (title) =>({
    type:'SET_ACC_TITLE',
    title
  })
  
  export { fetchList, fetchListSuccess, fetchListError, setTimelineWidth, resetTimelineWidth, setAccordionContent,setAccTitle };
  