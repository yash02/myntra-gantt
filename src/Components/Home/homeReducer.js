const initialState = {
    isLoading: false,
    data: {},
    error: null,
    timelineWidth:0,
    content:'',
    title:'',
  };
  
  const homeReducer = (state = initialState, action) => {
    switch (action.type) {
      case "FETCH_LIST":
        return { ...state, ...action, isLoading: true };

      case "FETCH_LIST_SUCCESS":
        return { ...state, ...action, isLoading: false };

      case "FETCH_LIST_ERROR":
        return { ...state, error: action.error, isLoading: false };

      case "SET_WIDTH":
        return { ...state, timelineWidth:action.width, isLoading: false };

      case "RESET_WIDTH":
        return { ...state, timelineWidth:0, isLoading: false };
      case "SET_ACCORDION_CONTENT":
          return { ...state, content:action.content, isLoading: false };
      case "SET_ACC_TITLE":
        console.log("content", action.title);
          return { ...state, title:action.title, isLoading: false };  
      default:
        return state;
    }
  };
  
  export default homeReducer;
  