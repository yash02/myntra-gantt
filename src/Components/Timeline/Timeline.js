import React, { Component } from "react";
import _ from "lodash";
import moment from "moment";
import "./Timeline.css";
import { connect } from "react-redux";
import Chevron from "../Chevron/Chevron";

class Timeline extends Component {

  getTick() {
    const { timelineWidth } = this.props;
    const maxTicks = 12;
    const width = timelineWidth / maxTicks;

    return {
      width: width,
      count: maxTicks
    };
  }

  timelineRender() {
    const tick = this.getTick();
    return (
      <div className="timeline">
        {_.map(_.range(tick.count), index => {
          return (
            <div
              key={`tick${index}`}
              style={{
                height: "20px",
                width: `${tick.width}px`,
                float: "left",
                margin: "0px",
                padding: "0px",
                textAlign: "left",
                paddingLeft: `${2}px`
              }}
            >
              {this.renderTickLabel(tick, index)}
              <span className="chevt">
                <Chevron width={5} fill={"#777"} />
              </span>
            </div>
          );
        })}
      </div>
    );
  }

  renderTickLabel(tick, index) {
    const l = this.props.data.startDate;
    const tickTime = moment(l).add(`${index}`, "M");
    const mname = this.month_name(new Date(tickTime.format("MM/DD/YYYY")));
    return mname;
  }

  month_name(dt) {
    const mlist = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "June",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec"
    ];
    return mlist[dt.getMonth()];
  }

  render() {
    return this.timelineRender();
  }
}

const mapStateToProps = state => {
  return {
    data: state.home.data,
    timelineWidth: state.home.timelineWidth
  };
};

const mapDispatchToProps = dispatch => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Timeline);
