import { takeEvery, call, put } from "redux-saga/effects";
import { getList } from '../services'
// import { fetchList, fetchListSuccess,fetchListError } from './Components/Home/homeAction'


export function* handleFetchList() {
  try {
    const res = yield call(getList);
    yield put({type: 'FETCH_LIST_SUCCESS', data: res.data});
  } catch (error) {
    yield put({type:'FETCH_LIST_ERROR', error:error.message});
  }
  return
}

export default function* watchFetchList() {
    yield takeEvery('FETCH_LIST', handleFetchList);
  }

