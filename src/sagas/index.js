import { all } from 'redux-saga/effects';
import watchFetchList from './homeSaga.js'

export default function* rootSaga() {
    yield all([watchFetchList()]);
}
